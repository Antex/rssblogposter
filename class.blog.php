<?php

	Abstract class Blog {
		private $_authorized;
		private $_http;
		private $_username;
		private $_password;
		private $_cache;
		private $_proxies;
		private $_ranges;
		
		public function is_authorized(){
			return $this->_authorized;
		}
		
		private function loadCache(){
			$cacheFile = tP(realpath(dirname(__FILE__)) . '/cache/' . __CLASS__ . '_' . $this->_username . '.dat'); 
			printl(__CLASS__ . " loading cache from '{$cacheFile}'" . PHP_EOL); 
			$cache = @file_get_contents($cacheFile);
			if(!empty($cache)){
				$this->_cache = unserialize($cache);
				if(count($this->_cache) > CACHE_LIMIT){
					printl(__CLASS__ . ' cache is full, clearing' . PHP_EOL);
					$this->_cache = array();
				}
			} else {
				$this->_cache = array();
			}
		}
		
		private function saveCache(){
			$cacheFile = tP(realpath(dirname(__FILE__)) . '/cache/' . __CLASS__ . '_' .	$this->_username . '.dat'); 
			file_put_contents($cacheFile, serialize($this->_cache));
		}
		
		public function __construct($username, $password, $ranges, $proxies){
			printl(__CLASS__ . " login ($username, $password)" . PHP_EOL);
			$this->_proxies = $proxies;
			$this->_username = $username; 
			$this->_password = $password;
			$this->_ranges = $ranges;
			$this->loadCache(); 
		}

		public function strip_only_tags($str, $tags, $stripContent=false) {
			$content = '';
			if(!is_array($tags)) {
				$tags = (strpos($str, '>') !== false ? explode('>', str_replace('<', '', $tags)) : array($tags));
				if(end($tags) == '') array_pop($tags);
			}
			foreach($tags as $tag) {
				if ($stripContent)
					 $content = '(.+</'.$tag.'(>|\s[^>]*>)|)';
				 $str = preg_replace('#</?'.$tag.'(>|\s[^>]*>)'.$content.'#is', '', $str);
			}
			return $str;
		}

		public function processText($input){
			$output = $this->strip_only_tags($input, STRIP_TAGS) . $this->getAppend();
			$output = preg_replace("/(\n)/", "", $output);

			$output = preg_replace('/(<br \/>){2,}/s', '<br />', $output);
			$output = preg_replace('/(<br \/>'.PHP_EOL.'){2,}/s', '<br />', $output);
			$output = preg_replace('/(<br>){2,}/s', '<br />', $output);
			$output = preg_replace('/(<br\/>){2,}/s', '<br />', $output);
			
			$out = explode(CUT_SEPARATOR, $output);
			if(count($out) > CUT_WORDS){
				printl(__CLASS__ . ' cutting text' . PHP_EOL);
				$out[CUT_WORDS] = $out[CUT_WORDS] . CUT_SEPARATOR . '<lj-cut text="' . $this->getCutText() . '">';
				$out[] = '</lj-cut>';
				$output = implode(CUT_SEPARATOR, $out);
			}
			return $output;
		}

		public function testProxy($proxy){
			printl(__CLASS__ . " testing proxy '{$login}{$proxy[host]}'... ");
			$px = $proxy['host'];
			if(!empty($proxy['login']))
				$pwd = $proxy['login'] . ':' . $proxy['pass']; 
			$ch = curl_init('http://checker.samair.ru/');
			curl_setopt($ch, CURLOPT_PROXY, $px);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			if(isset($pwd))
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $pwd);
			$page = curl_exec($ch);
			curl_close($ch);
			$ip = explode(":", $px);
			$ip = $ip[0];
			if(strpos($page, $ip) !== false && strpos($page, 'Your IP is detected.') === false){
				printl('good' . PHP_EOL);
				return $px;
			} else {
				printl('bad' . PHP_EOL);
				return false;
			}
		}
		
		public function getProxy(){
			if(!count($this->_proxies)){
				printl('no proxies :(' . PHP_EOL);
				return false;
			}
			foreach($this->_proxies as $proxy){
				if(PROXY_RAND == '1')
					$proxy = $this->_proxies[rand(0, count($this->_proxies) -1)];
				$login = $proxy['login'] ? $proxy['login'] . '@' : '';
				if($px = $this->testProxy($proxy))
					return $px;
			}
			return false;
		}
		
		public function convertEncoding($text){
			$enc = detect_encoding($text);
			if($enc != 'utf-8'){
				$text = iconv($enc, 'utf-8', $text);
			}
			return $text;
		}
		
		public function getAppend(){
			$f = @file(tP(POST_APPEND));
			return $this->convertEncoding(trim($f[rand(0, count($f)-1)]));
		}
		
		public function getCutText(){
			$f = @file(tP(CUT_TEXT));
			return $this->convertEncoding(trim($f[rand(0, count($f)-1)]));
		}
		
		
		public function post($news){
			return true;
		}

}