<?php
	define('DEBUG', false);
  
	error_reporting(E_ERROR);
	ini_set('display_errors', 'On');
	ini_set('display_startup_errors', 'On');
  
  	//require_once 'class.blog.php';
	require_once 'class.lj.php';
	require_once 'class.keywords.php';
  
	include 'init.php';
	
	function checkTime($startTime, $endTime){
		global $cHour;
		global $cMin;
		global $sHour;
		global $sMin;
		global $eHour;
		global $eMin;
		$cHour = intval(date("H"));
		$cMin = intval(date("i"));
		$start = explode(":",$startTime);
		$sHour = intval($start[0]);	
		$sMin = intval($start[1]);
		$end = explode(":",$endTime);
		$eHour = intval($end[0]);	
		$eMin = intval($end[1]);	
		$pass = true;
		if($sHour <= $eHour){
			if($cHour < $sHour){
				$pass = false;
			};
			if($cHour > $eHour){
				$pass = false;
			};
			if($cHour == $sHour){
				if($cMin < $sMin){
					$pass = false;
				};
			};
			if($cHour == $eHour){
				if($cMin > $eMin){
					$pass = false;
				};
			};
		} else {
			if( ($cHour < $sHour) && ($cHour > $eHour) ){
				$pass = false;
			};
			if($cHour == $sHour){
				if($cMin < $sMin){
					$pass = false;
				};
			};
			if($cHour == $eHour){
				if($cMin > $eMin){
					$pass = false;
				};
			};

		};
		return $pass;
	};

	function loadRanges($rfile){
		if(!file_exists($rfile)){
			die(printl("failed to open '{$rfile}'" . PHP_EOL));
		}
		$ranges = array();
		$f = @file($rfile);
		foreach($f as $string){
			$i++;
			$string = explode("|", $string);
			if(count($string) != 2){
				printl("error parsing '{$rfile}' at line $i" . PHP_EOL);
				continue;
			}
			$ranges[$string[0]] = parseRanges($string[1]);
		}
		return $ranges;
	}
	
	function parseRanges($in){
		$ranges = array();
		$mask = "/([0-9]{2}):([0-9]{2})-([0-9]{2}):([0-9]{2})/s";
		preg_match_all($mask, $in, $matches);
		foreach($matches[0] as $i => $m){
			$range = array(
				'hf' => $matches[1][$i],
				'mf' => $matches[2][$i],
				'ht' => $matches[3][$i],
				'mt' => $matches[4][$i]
			);
			$ranges[] = $range;
		}
		return $ranges;
	}

	function inRanges($ranges){
		if(!is_array($ranges)){
			printl("time ranges is not set" . PHP_EOL);
			return false;
		}
		foreach($ranges as $range){
			if(checkTime("{$range[hf]}:{$range[mf]}", "{$range[ht]}:{$range[mt]}"))
				return true;
		}
		return false;
	}
	
	function loadTasks($tasks_file){
		$tasks = array();
		if($f = @file($tasks_file)){
			$task = array();
			foreach($f as $s){
				if($s[0] == "#")
					continue;
				$s = explode('|', chop($s));
				switch($s[0]){
					case 'rss':
						$task['rss'][] = $s[1];
						break;
					
					case 'blog':
						$task['blog'][] = array('login' => $s[1], 'pass' => $s[2]);
						break;	
					
					default:
						$tasks[] = $task;
						$task = array();
						break;
				}
			}
			if(count($task))
				$tasks[] = $task;
		} else {
			die(printl("failed to open {$tasks_file}\n"));
		}
		return $tasks;
	}
	

	function loadArticles($path){
		$ret = array();
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				if ($file != "." && $file != "..") {
					$content = file_get_contents(tP($path . $file));
					$enc = detect_encoding($content);
					if($enc != 'utf-8'){
						$content = iconv($enc, 'utf-8', $content);
					}
					$mask = '/<title>(.*?)<\/title>/si';
					if(preg_match($mask, $content, $matches)){
						$title = $matches[1];
						$content = str_replace($matches[0], '', $content);
					} else {
						$mask = '/<h1>(.*?)<\/h1>/si';
						if(preg_match($mask, $content, $matches)){
							$title = $matches[1];
							$content = str_replace($matches[0], '', $content);
						} else {
							$title = pathinfo($file, PATHINFO_FILENAME);
						}
					}
					$ret[] = array(
						'title' => $title,
						'description' => $content,
						'fname' => tP($path . $file)
					);
				}
			}
			closedir($handle);
		}	
		return $ret;
	}
	
	function loadProxy($proxy_file){
		$proxies = array();
		if($f = @file($proxy_file)){	
			foreach($f as $s){
				$s = explode('|', trim($s));
				if(strpos($s[1], 'worldofproxy.com') !== false){
					$url = trim($s[1]);
					$from = intval($s[2]);
					$to = intval($s[3]);
					printl("fetching proxylist from '{$url}'... ");
					$proxylist = file_get_contents($url);
					$cnt = 0;
					$i = 0;
					if(!empty($proxylist)){
						$proxylist = explode("\n", $proxylist);
						foreach($proxylist as $proxy){
							if($i >= $from && $i <= $to){
								$cnt++;
								$proxies[$s[0]][] = array('host' => trim($proxy), 'login' => '', 'pass' => '');
							}
							$i++;
						}
					}
					printl(" loaded {$cnt} proxies." . PHP_EOL);
				} else {
					$proxies[$s[0]][] = array('host' => $s[1], 'login' => trim($s[2]), 'pass' => trim($s[3]));
				}
			}
		} else {
			die(printl("failed to open {$proxy_file}" . PHP_EOL));
		}
		return $proxies;
	}
	
	
	function postToBlog($blog, $news, $ranges, $proxies){
		$ranges = $ranges[$blog['login']];
		$proxies = $proxies[$blog['login']];
		//$kw = new Keywords();
		//printl($kw->findKeywords($news['description']));
		$blog = new LiveJournal($blog['login'], $blog['pass'], $ranges, $proxies);
		return $blog->post($news);
	}
	
	function loadArticle($articles, $blog, $ranges, $proxies){
		$art_cache = unserialize(@file_get_contents(tP(ARTICLES_CACHE)));
		foreach($articles as $i => $article){
			if($article['removed'])
				continue;
			$key = $blog['login'] . '-' . date("Y-m-d-h");
			printl("posting article '{$article[title]}' to blog {$blog[login]}... ");
			if($art_cache['counter'][$key] >= ARTICLE_PER_HOUR){
				printl("skiped, article posting limit reached" . PHP_EOL);
				continue;
			}
			$k = md5($article['title']);
			if(!in_array($k, $art_cache)){
				printl(PHP_EOL);
				if(postToBlog($blog, $article, $ranges, $proxies)){
					printl("removing article..." . PHP_EOL);
					@unlink($articles[$i]['fname']);
					$art_cache['counter'][$key] ++;
					$art_cache[] = $k;
				}
			}
		}
		file_put_contents(tP(ARTICLES_CACHE), serialize($art_cache));
	}
	
	function loadRss($rss, $blogs, $ranges, $proxies){
		printl("loading new RSS '$rss' ..." . PHP_EOL);
		$rss_cache = unserialize(@file_get_contents(tP(RSS_CACHE)));
		$xml_file = file_get_contents($rss);
		$enc = detect_encoding($xml_file);
		if($enc != 'utf-8'){
			printl("converting to UTF-8" . PHP_EOL);
			$xml_file = iconv($enc, 'utf-8', $xml_file);
		}
		$xml = simplexml_load_string($xml_file);	
		$blogs_count = count($blogs);
		$i = 0;
		$items = (array)$xml->channel;
		$items = array_reverse($items['item']);
		foreach ($items as $item) {
			$news = array(
					'title' => (string) $item->title,
					'description' => (string) $item->description,
					'link' => (string) $item->link,
					'date' => (string) $item->pubDate
				);
			
			if($c = $rss_cache[md5($rss)][$news['date']]){
				$i = $c['i'];
				if($c['done']){
					$i++;
					printl($i . '> skiped: ' . $news['title'] . ' (' . $news['date'] . ')' . PHP_EOL);
					continue;
				}
			}
			
			$c = $i % $blogs_count;
			$blog = $blogs[$c];
			
			$done = postToBlog($blog, $news, $ranges, $proxies);
			$rss_cache[md5($rss)][$news['date']]['i'] = $i;
			$rss_cache[md5($rss)][$news['date']]['done'] = $done;
			file_put_contents(tP(RSS_CACHE), serialize($rss_cache));
			
			$r = rand(WAIT_F, WAIT_T);
			if($r > 0)
				printl("waiting {$r} sec..." . PHP_EOL);
			sleep($r);
			$i++;			
		}
	}
	
	$date = date("Y-m-d H:i:s");
	printl("[{$date}][{$report}][" . OS . "] starting..." . PHP_EOL);
	$tasks = loadTasks(TASKS_FILE);
	$proxies = loadProxy(PROXY_FILE);
	$articles = loadArticles(ARTICLES_PATH);
	$ranges = loadRanges(RANGES_FILE);

	foreach($tasks as $task){
		foreach($task['blog'] as $blog){
			loadArticle(@$articles, $blog, $ranges, $proxies);
		}
	}
	
	foreach($tasks as $task){
		foreach($task['rss'] as $rss){
			loadRss($rss, $task['blog'], $ranges, $proxies);
		}
	}
	
	