<?php

	function detect_encoding($string) {  
		static $list = array('utf-8', 'windows-1251');
		foreach ($list as $item) {
			$sample = iconv($item, $item, $string);
			if (md5($sample) == md5($string))
				return $item;
		}
		return null;
	}
  
	function printl($s){
		global $report;
		$dir = 'logs/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
		if(!is_dir(tP($dir))){
			$oldumask = umask(0);
			@mkdir(tP($dir), 0777, true);
			umask($oldumask);
		}
		file_put_contents(tP($dir . 'report' . $report . '.txt'), $s, FILE_APPEND);
		if(OS == 'WIN32'){
			$enc = detect_encoding($s);
			$s = iconv($enc, 'cp1251', $s);
		}
		print $s;
	}
	
  function tP($path, $reverse = false){
		$npath = $path;
		if(OS == "WIN32"){
			if(!$reverse)
				$npath = str_replace("/", "\\", $path);
			else
				$npath = str_replace("\\", "/", $path);
		}
		if(DEBUG)
			printl("transPath: $path -> $npath" . PHP_EOL);
		return $npath;
	}
	
	if(file_exists('/etc/passwd')){
		define('OS', 'UNIX');
	} else {
		define('OS', 'WIN32');
	}

	if(OS == 'WIN32'){
		system("chcp 1251>NUL");
		setlocale(LC_ALL, 'ru_RU.CP1251', 'rus_RUS.CP1251', 'Russian_Russia.1251');
		//setlocale(LC_ALL, "en_us.CP1251");
		dl('php_curl.dll');
	}
	
	$dir = 'logs/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
	$report = '';
	while(file_exists(tP($dir . 'report' . $report . '.txt'))){
		$report ++;
	}
	
	if($f = @file('config.txt')){
		$l = 0;
		foreach($f as $string){
			$l++;
			$string = trim($string);
			if($string[0] == '#' || strlen($string) == 0)
				continue;
			$param = explode("=", $string);
			if(count($param) == 1){
				printl("error parsing config file at line {$l}" . PHP_EOL);
			} else if(count($param) > 2){
				$param[1] = implode("=", array_slice($param, 1));
			}
			$key = chop($param[0]);
			$value = str_replace("'", "", trim($param[1]));
			define($key, $value);
		}
	} else {
		die("config.txt not exists" . PHP_EOL);
	}
	