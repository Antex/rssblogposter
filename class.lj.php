<?php

	class LiveJournal {
		private $_authorized;
		private $_http;
		private $_username;
		private $_password;
		private $_cache;
		private $_proxies;
		private $_ranges;
		
		public function is_authorized(){
			return $this->_authorized;
		}
		
		private function loadCache(){
			$cacheFile = tP(realpath(dirname(__FILE__)) . '/cache/' . __CLASS__ . '_' . $this->_username . '.dat'); 
			printl(__CLASS__ . " loading cache from '{$cacheFile}'" . PHP_EOL); 
			$cache = @file_get_contents($cacheFile);
			if(!empty($cache)){
				$this->_cache = unserialize($cache);
				if(count($this->_cache) > CACHE_LIMIT){
					printl(__CLASS__ . ' cache is full, clearing' . PHP_EOL);
					$this->_cache = array();
				}
			} else {
				$this->_cache = array();
			}
		}
		
		private function saveCache(){
			$cacheFile = tP(realpath(dirname(__FILE__)) . '/cache/' . __CLASS__ . '_' .	$this->_username . '.dat'); 
			file_put_contents($cacheFile, serialize($this->_cache));
		}
		
		public function __construct($username, $password, $ranges, $proxies){
			printl(__CLASS__ . " login ($username, $password)" . PHP_EOL);
			$this->_proxies = $proxies;
			$this->_username = $username; 
			$this->_password = $password;
			$this->_ranges = $ranges;
			$this->loadCache(); 
		}

		public function strip_only_tags($str, $tags, $stripContent=false) {
			$content = '';
			if(!is_array($tags)) {
				$tags = (strpos($str, '>') !== false ? explode('>', str_replace('<', '', $tags)) : array($tags));
				if(end($tags) == '') array_pop($tags);
			}
			foreach($tags as $tag) {
				if ($stripContent)
					 $content = '(.+</'.$tag.'(>|\s[^>]*>)|)';
				 $str = preg_replace('#</?'.$tag.'(>|\s[^>]*>)'.$content.'#is', '', $str);
			}
			return $str;
		}

		public function removeLinks($text){
			$text2 = strip_tags($text);
			preg_match_all('@^(?:http://)?([^/]+)@i', $text2, $matches);
			$text = str_replace($matches[0], '', $text);
			return $text;
		}
	
		public function processText($input){
			$output = $this->strip_only_tags($input, STRIP_TAGS) . $this->getAppend();
			$output = preg_replace("/(\n)/", "", $output);

			$output = preg_replace('/(<br \/>){2,}/s', '<br />', $output);
			$output = preg_replace('/(<br \/>'.PHP_EOL.'){2,}/s', '<br />', $output);
			$output = preg_replace('/(<br>){2,}/s', '<br />', $output);
			$output = preg_replace('/(<br\/>){2,}/s', '<br />', $output);
			
			$output = str_replace($this->getBlacklist(), '', $output);
			$output = $this->removeLinks(output);
			$out = explode(CUT_SEPARATOR, $output);
			if(count($out) > CUT_WORDS){
				//printl(__CLASS__ . ' cutting text' . PHP_EOL);
				$out[CUT_WORDS] = $out[CUT_WORDS] . CUT_SEPARATOR . '<lj-cut text="' . $this->getCutText() . '">';
				$out[] = '</lj-cut>';
				$output = implode(CUT_SEPARATOR, $out);
			}
			return $output;
		}

		public function testProxy($proxy){
			printl(__CLASS__ . " testing proxy '{$login}{$proxy[host]}'... ");
			$px = $proxy['host'];
			if(!empty($proxy['login']))
				$pwd = $proxy['login'] . ':' . $proxy['pass']; 
			/*
			$ch = curl_init('http://checker.samair.ru/');
			curl_setopt($ch, CURLOPT_PROXY, $px);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			if(isset($pwd))
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $pwd);
			$page = curl_exec($ch);
			curl_close($ch);
			$ip = explode(":", $px);
			$ip = $ip[0];
			if(strpos($page, $ip) !== false && strpos($page, 'Your IP is detected.') === false){
				printl('good' . PHP_EOL);
				return $px;
			} else {
				printl('bad' . PHP_EOL);
				return false;
			}
			*/
			switch(PROXY_TEST){
				case '1':
					$ch = curl_init('http://www.all-nettools.com/toolbox/proxy-test.php');
					curl_setopt($ch, CURLOPT_PROXY, $px);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_TIMEOUT, PROXY_TEST_TIMEOUT); 
					if(isset($pwd))
						curl_setopt($ch, CURLOPT_PROXYUSERPWD, $pwd);
					$page = curl_exec($ch);
					curl_close($ch);
					$ip = explode(":", $px);
					$ip = $ip[0];
					if(strpos($page, 'You came from') === false && strpos($page, 'Proxy Test') !== false){
						printl('good' . PHP_EOL);
						return $proxy;
					} else {
						printl('bad' . PHP_EOL);
						return false;
					}
				case '0':
					$ch = curl_init('http://www.google.com');
					curl_setopt($ch, CURLOPT_PROXY, $px);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_TIMEOUT, PROXY_TEST_TIMEOUT); 
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); 
					if(isset($pwd))
						curl_setopt($ch, CURLOPT_PROXYUSERPWD, $pwd);
					$page = curl_exec($ch);
					curl_close($ch);
					if(strpos($page, '<title>Google</title>') !== false){
						printl('good' . PHP_EOL);
						return $proxy;
					} else {
						printl('bad' . PHP_EOL);
						return false;
					}
			}
			return false;
		}
		
		public function getProxy($new = false){
			$proxyCache = unserialize(@file_get_contents(tP(PROXY_CACHE)));
			if(!empty($proxyCache[$this->_username]) && !$new){
				if($px = $this->testProxy($proxyCache[$this->_username])){
					return $px;
				}
			}
			if(!count($this->_proxies)){
				printl('no proxies :(' . PHP_EOL);
				return false;
			}
			foreach($this->_proxies as $proxy){
				if(PROXY_RAND == '1')
					$proxy = $this->_proxies[rand(0, count($this->_proxies) -1)];
				$login = $proxy['login'] ? $proxy['login'] . '@' : '';
				if($px = $this->testProxy($proxy)){
					$proxyCache[$this->_username] = $proxy;
					file_put_contents(tP(PROXY_CACHE), serialize($proxyCache));
					return $px;
				}
			}
			return false;
		}
		
		public function convertEncoding($text){
			$enc = detect_encoding($text);
			if($enc != 'utf-8'){
				$text = iconv($enc, 'utf-8', $text);
			}
			return $text;
		}
		
		public function getBlacklist(){
			$f = @file(tP(BLACK_LIST));
			$bl = array();
			foreach($f as $s)
				$bl[] = $this->convertEncoding(trim($s));
			return $bl;
		}
		
		public function getAppend(){
			$f = @file(tP(POST_APPEND));
			return $this->convertEncoding(trim($f[rand(0, count($f)-1)]));
		}
		
		public function getCutText(){
			$f = @file(tP(CUT_TEXT));
			return $this->convertEncoding(trim($f[rand(0, count($f)-1)]));
		}
		
		
		public function post($news, $try = 1){
			printl(__CLASS__ . " [try {$try}] posting news '{$news[title]}'" . PHP_EOL);
			$title = $news['title']; 
			$text = $this->processText($news['description']);
			if(in_array($news['title'], $this->_cache)){
				printl(__CLASS__ . " skiped" . PHP_EOL);
				return false;
			}
			if(!inRanges($this->_ranges)){
				printl(__CLASS__ . " not in time ranges, skiped" . PHP_EOL);
				return false;
			}
			$proxy = $this->getProxy($try > 1);
			if(!$proxy){
				printl(__CLASS__ . " no proxy" . PHP_EOL);
				return false;
			}
			$year = date("Y"); 
			$month = date("m"); 
			$day = date("d"); 
			$hour = date("H"); 
			$min = date("i"); 

			$query = '<?xml version="1.0"?> 
			<methodCall> 
			  <methodName>LJ.XMLRPC.postevent</methodName> 
			  <params> 
				<param> 
				  <value> 
					<struct> 
					  <member> 
						<name>username</name> 
						<value> 
						  <string>'.$this->_username.'</string> 
						</value> 
					  </member> 
					  <member> 
						<name>password</name> 
						<value> 
						  <string>'.$this->_password.'</string> 
						</value> 
					  </member> 
					  <member> 
						<name>event</name> 
						<value> 
						  <string><![CDATA['.$text.']]></string> 
						</value> 
					  </member> 
					  <member> 
						<name>subject</name> 
						<value> 
						  <string><![CDATA['.$title.']]></string> 
						</value> 
					  </member> 
					  <member> 
						<name>lineendings</name> 
						<value> 
						  <string>pc</string> 
						</value> 
					  </member> 
					  <member> 
						<name>year</name> 
						<value> 
						  <int>'.$year.'</int> 
						</value> 
					  </member> 
					  <member> 
						<name>mon</name> 
						<value> 
						  <int>'.$month.'</int> 
						</value> 
					  </member> 
					  <member> 
						<name>day</name> 
						<value> 
						  <int>'.$day.'</int> 
						</value> 
					  </member> 
					  <member> 
						<name>hour</name> 
						<value> 
						  <int>'.$hour.'</int> 
						</value> 
					  </member> 
					  <member> 
						<name>min</name> 
						<value> 
						  <int>'.$min.'</int> 
						</value> 
					  </member> 
					</struct> 
				  </value> 
				</param> 
			  </params> 
			</methodCall>';
			
			printl(__CLASS__ . " posting... ");
			
			$headers = array();
			$headers[] = "Host: www.livejournal.com";  
			$headers[] = "User Agent: XMLRPC Client 1.0";  
			$headers[] = "Connection: Close";  
			$headers[] = "Content-Type: text/xml";  
			$headers[] = "Content-Length:". strlen($query) ."\r\n"; 
			$headers[] = $query;
			
			$px = $proxy['host'];
			$ch = curl_init("http://www.livejournal.com/interface/xmlrpc");
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
			curl_setopt( $ch, CURLOPT_PROXY, $px );
			curl_setopt( $ch, CURLOPT_TIMEOUT, POST_TIMEOUT); 
			if(!empty($proxy['login']))
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxy['login'] . ':' . $proxy['pass']);
			$answer = curl_exec( $ch );
			curl_close($ch); 
			
			if(empty($answer)){
				printl(" failed" . PHP_EOL);
				if($try == POST_TRIES)
					return false;
				return $this->post($news, $try + 1);
			}
				
			$answer = simplexml_load_string($answer);
			$member_id = 0;
			$faultString = '';
			if(isset($answer->fault->value->struct->member)){
				printl("fail." . PHP_EOL);
				foreach($answer->fault->value->struct->member as $member){
					if($member->name == 'faultString')
						$faultString = $member->value->string;
				}
				if(!empty($faultString)){
					printl(__CLASS__ . " username '{$this->_username}' gives error '$faultString'" . PHP_EOL);
				}
				return false;
			}
			printl("done." . PHP_EOL);
			$this->_cache[] = $news['title'];
			$this->saveCache();
			return true;
		}

}