<?php
	class HttpClient {
		protected $_cookies;
		protected $_curlh;

		public function setCookies($name, $value){
			$this->_cookies[] = "{$name}={$value}";
		}

		public function request($url, $method="GET"){
			$this->_curlh = curl_init();
			curl_setopt($this->_curlh, CURLOPT_URL, $url);
			curl_setopt($this->_curlh, CURLOPT_FAILONERROR, true);
			curl_setopt($this->_curlh, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($this->_curlh, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($this->_curlh, CURLOPT_HEADER, true);
			curl_setopt($this->_curlh, CURLOPT_POST, false);
			if($this->_cookies){
				$cookies = implode("; ", $this->_cookies);
				$headers[] ='Cookie: ' . $cookies;
				curl_setopt($this->_curlh, CURLOPT_HTTPHEADER, $headers);
			}
			$result = curl_exec($this->_curlh);
			curl_close($this->_curlh);
			return $result;
		}

	}
